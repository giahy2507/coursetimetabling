import openpyxl
from openpyxl import Workbook, load_workbook
from collections import OrderedDict
import numpy as np
import pickle
import copy
from demo_problem import *

class PhongHoc(object):
    col_infor = OrderedDict()
    def __init__(self, day, tenphong, succhua, maychieu, thuchanh, ghichu, tinhchat, nhomph):
        self.day = str(day)
        self.tenphong = str(tenphong)
        self.succhua = int(succhua)
        self.maychieu = True if maychieu else False
        self.thuchanh = True if thuchanh else False
        self.ghichu = str(ghichu) if ghichu else ""
        self.tinhchat = str(tinhchat) if tinhchat else ""
        self.nhomph = int(nhomph)

    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "day":
                result["day"] = idx
            elif col.value == "tenph":
                result["tenph"] = idx
            elif col.value == "succhua":
                result["succhua"] = idx
            elif col.value == "maychieu":
                result["maychieu"] = idx
            elif col.value == "ph":
                result["ph"] = idx
            elif col.value == "thuchanh":
                result["thuchanh"] = idx
            elif col.value == "ghichu":
                result["ghichu"] = idx
            elif col.value == "tinhchat":
                result["tinhchat"] = idx
            elif col.value == "nhomph":
                result["nhomph"] = idx
        self.col_infor = result
        return result

    @classmethod
    def PhongHoc_from_row(self, row):
        day = row[self.col_infor["day"]].value
        tenphong = row[self.col_infor["tenph"]].value
        succhua = row[self.col_infor["succhua"]].value
        maychieu = row[self.col_infor["maychieu"]].value
        thuchanh = row[self.col_infor["thuchanh"]].value
        ghichu = row[self.col_infor["ghichu"]].value
        tinhchat = row[self.col_infor["tinhchat"]].value
        nhomph = row[self.col_infor["nhomph"]].value
        return PhongHoc(day, tenphong, succhua,maychieu,thuchanh,ghichu,tinhchat,nhomph)

    @classmethod
    def load_list_PhongHoc(cls, worksheet):
        list_PhongHoc = list()
        list_PhongHoc.append(PhongHoc("Dummy","Dummy", 0, None, None, None, None, -1))
        dict_PhongHoc = OrderedDict()
        dict_NhomPhongHoc = OrderedDict()
        ws_phonghoc_rows = worksheet.rows
        first_row = next(ws_phonghoc_rows)
        second_row = next(ws_phonghoc_rows)
        col_infor_row = next(ws_phonghoc_rows)
        PhongHoc.detect_col(col_infor_row)
        print(PhongHoc.col_infor)
        for row in ws_phonghoc_rows:
            ph = PhongHoc.PhongHoc_from_row(row)
            list_PhongHoc.append(ph)
            dict_PhongHoc[ph.tenphong] = ph

        for idx, ph in enumerate(list_PhongHoc):
            if ph.nhomph not in dict_NhomPhongHoc.keys():
                dict_NhomPhongHoc[ph.nhomph] = []
            dict_NhomPhongHoc[ph.nhomph].append(idx)

        return list_PhongHoc, dict_PhongHoc, dict_NhomPhongHoc

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}, {5}, {6}".format(self.day, self.tenphong, self.succhua, self.maychieu, self.thuchanh, self.ghichu, self.tinhchat)

class MonHoc(object):
    col_infor = OrderedDict()
    def __init__(self, mamh, tenmh, dvht, ts, lt, bt, th, tiettkb, nganh, tinhchat, batbuoc, nhomph, moriengcntn):
        self.mamh = str(mamh)
        self.tenmh = str(tenmh)
        self.dvht = int(dvht) if dvht and str(dvht) != "NULL" else 0
        self.ts = int(ts) if ts and str(ts) != "NULL" else 0
        self.lt = int(lt) if lt and str(lt) != "NULL" else 0
        self.bt = int(bt) if bt and str(bt) != "NULL" else 0
        self.th = int(th) if th and str(th) != "NULL" else 0
        self.tiettkb = int(tiettkb) if tiettkb and str(tiettkb) != "NULL" else 0
        # mon nao co so tiet thi gan bang so tiet, neu khong co so tiet thi mac dinh so tiet min
        self.nganh = str(nganh)
        self.tinhchat = str(tinhchat) if tinhchat else ""
        self.batbuoc = True if batbuoc else False
        self.nhomph = nhomph
        self.moriengcntn = True if moriengcntn else False
        self.list_giodk = []
        self.list_phonghoccu = []
        self.solopmo = 0

    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "mamh":
                result["mamh"] = idx
            elif col.value == "tenmh":
                result["tenmh"] = idx
            elif col.value == "dvht":
                result["dvht"] = idx
            elif col.value == "ts":
                result["ts"] = idx
            elif col.value == "lt":
                result["lt"] = idx
            elif col.value == "bt":
                result["bt"] = idx
            elif col.value == "th":
                result["th"] = idx
            elif col.value == "tiettkb":
                result["tiettkb"] = idx
            elif col.value == "nganh":
                result["nganh"] = idx
            elif col.value == "tinhchat":
                result["tinhchat"] = idx
            elif col.value == "batbuoc":
                result["batbuoc"] = idx
            elif col.value == "nhomph":
                result["nhomph"] = idx
            elif col.value == "moriengcntn":
                result["moriengcntn"] = idx
        self.col_infor = result
        return result

    @classmethod
    def MonHoc_from_row(self, row):
        mamh = row[self.col_infor["mamh"]].value
        tenmh = row[self.col_infor["tenmh"]].value
        dvht = row[self.col_infor["dvht"]].value
        ts = row[self.col_infor["ts"]].value
        lt = row[self.col_infor["lt"]].value
        bt = row[self.col_infor["bt"]].value
        th = row[self.col_infor["th"]].value
        tiettkb = row[self.col_infor["tiettkb"]].value
        nganh = row[self.col_infor["nganh"]].value
        tinhchat = row[self.col_infor["tinhchat"]].value
        batbuoc = row[self.col_infor["batbuoc"]].value
        nhomph = row[self.col_infor["nhomph"]].value
        moriengcntn = row[self.col_infor["moriengcntn"]].value
        # if mamh=="AN005":
        #     print("ahihi")
        if tiettkb == "NULL" and tinhchat == "NULL":
            # print(tenmh)
            return None
        return MonHoc(mamh,tenmh,dvht,ts,lt,bt,th,tiettkb,nganh,tinhchat,batbuoc,nhomph,moriengcntn)

    @classmethod
    def load_list_MonHoc(cls, worksheet):
        list_MonHoc = list()
        ws_monhoc_rows = worksheet.rows
        first_row = next(ws_monhoc_rows)
        second_row = next(ws_monhoc_rows)
        col_infor_row = next(ws_monhoc_rows)
        dict_MonHoc = OrderedDict()
        MonHoc.detect_col(col_infor_row)
        print(MonHoc.col_infor)
        for row in ws_monhoc_rows:
            monhoc = MonHoc.MonHoc_from_row(row)
            if monhoc:
                if monhoc.tiettkb == 0:
                    continue
                list_MonHoc.append(monhoc)
                dict_MonHoc[monhoc.mamh] = monhoc
        return list_MonHoc, dict_MonHoc

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}, {5}, {7}, {8}, {9}, {10}".format(self.mamh,
                                                               self.tenmh,
                                                               self.dvht,
                                                               self.ts,
                                                               self.lt,
                                                               self.bt,
                                                               self.th,
                                                               self.tiettkb,
                                                               self.nganh,
                                                               self.tinhchat,
                                                                self.batbuoc)

class Lop(object):
    col_infor = OrderedDict()
    def __init__(self, kh, nganh, lop, loailop, soluongSV):
        self.kh = str(kh)
        self.nganh = str(nganh)
        self.lop = str(lop)
        self.loailop = str(loailop) if loailop else "CQ"
        self.soluongSV = int(soluongSV)
        self.listmonhoc = list()


    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "kh":
                result["kh"] = idx
            elif col.value == "nganh":
                result["nganh"] = idx
            elif col.value == "lop":
                result["lop"] = idx
            elif col.value == "loailop":
                result["loailop"] = idx
            elif col.value == "soluongSV":
                result["soluongSV"] = idx
        self.col_infor = result
        return result

    @classmethod
    def Lop_from_row(self, row):
        kh = row[self.col_infor["kh"]].value
        nganh = row[self.col_infor["nganh"]].value
        lop = row[self.col_infor["lop"]].value
        loailop = row[self.col_infor["loailop"]].value
        soluongSV = row[self.col_infor["soluongSV"]].value
        return Lop(kh,nganh,lop,loailop,soluongSV)


    @classmethod
    def load_list_Lop(cls, worksheet):
        list_Lop = list()
        ws_lop_rows = worksheet.rows
        first_row = next(ws_lop_rows)
        second_row = next(ws_lop_rows)
        col_infor_row = next(ws_lop_rows)
        Lop.detect_col(col_infor_row)
        dict_Lop = OrderedDict()
        for row in ws_lop_rows:
            lop = Lop.Lop_from_row(row)
            if lop:
                if lop.lop not in dict_Lop.keys():
                    dict_Lop[lop.lop] = OrderedDict()
                dict_Lop[lop.lop][lop.loailop] = lop
                list_Lop.append(lop)
        return list_Lop, dict_Lop

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}".format(self.kh, self.nganh, self.lop, self.loailop, self.soluongSV)

class GiangVien(object):
    def __init__(self, magv, tengv, mamh, list_giodk=[]):
        self.magv = magv
        self.tengv = tengv
        self.mamh = mamh
        self.list_giodk = list_giodk
        self.list_gioday = []
        self.list_gioranh = list_giodk

    @classmethod
    def load_list_GiangVien(self, worksheet):
        list_giangvien = []
        dict_giangvien = OrderedDict()
        ws_giangvien_rows = worksheet.rows
        first_row = next(ws_giangvien_rows)
        second_row = next(ws_giangvien_rows)
        third_row = next(ws_giangvien_rows)
        for row in ws_giangvien_rows:
            magv = row[0].value
            tengv = row[1].value
            mamh = row[2].value
            list_giangvien.append(GiangVien(magv, tengv, mamh, []))

        for giangvien in list_giangvien:
            if giangvien.magv not in dict_giangvien.keys():
                dict_giangvien[giangvien.magv] = giangvien
        return list_giangvien, dict_giangvien

    @classmethod
    def load_giodk_GiangVien(cls, dict_giangvien, dict_monhoc, worksheet):
        ws_giodk_rows = worksheet.rows
        first_row = next(ws_giodk_rows)
        second_row = next(ws_giodk_rows)
        third_row = next(ws_giodk_rows)
        for row in ws_giodk_rows:
            magv = row[0].value
            thu = (row[1].value - 2)*10
            buoi = row[2].value
            if buoi == "S":
                giodk = [ thu + i for i in range(5)]
            else:
                giodk = [ thu + i for i in range(5,10)]
            dict_giangvien[magv].list_giodk+=giodk
            mamh = dict_giangvien[magv].mamh
            dict_monhoc[mamh].list_giodk += giodk
        return dict_giangvien, dict_monhoc

    @classmethod
    def generate_data(cls, dict_MonHoc):
        list_GiangVien = []
        list_magv = list(range(1000))
        np.random.shuffle(list_magv)
        list_magv = iter(list_magv)
        for mamh in dict_MonHoc.keys():
            monhoc = dict_MonHoc[mamh]
            if monhoc.batbuoc == True:
                no_gv = np.random.randint(6,7)
                for i in range(no_gv):
                    magv = next(list_magv)
                    no_giodk = np.random.randint(3,4)
                    list_giodk = []
                    for j in range(no_giodk):
                        random_date = np.random.randint(0,6)
                        random_sc = np.random.randint(0,2)
                        aaa = list(range(random_date*10+random_sc*5,random_date*10+random_sc*5+5,1))
                        if aaa[0] not in list_giodk:
                            list_giodk+=aaa
                    list_GiangVien.append(GiangVien(magv,"",monhoc.mamh,list_giodk))
                    dict_MonHoc[mamh].list_giodk+=list_giodk
        return list_GiangVien, dict_MonHoc

class PhanCong:

    dict_monhoc_assignbeid = {}
    dict_monhocbb2_random_available_tietbd = {}

    def __init__(self, monhoc, lop, soluongtb):
        self.monhoc = monhoc
        self.lop = lop
        self.soluongtb = soluongtb
        self.tiettkb = self.monhoc.tiettkb
        self.soluongSV = lop.soluongSV
        self.available_tietbd = sorted(self.generate_available_tietbd())
        self.noBE = len(self.available_tietbd)
        self.list_beid = None
        self.list_assignbeid = []
        self.available_phonghoc = []
        self.nhom_phonghoc = 0

    def generate_available_tietbd(self):
        tiettkb = self.monhoc.tiettkb
        list_giodk = self.monhoc.list_giodk
        result = None
        if len(list_giodk) == 0 :
            list_giodk = list(range(0,60))
        if tiettkb == 10:
            result = [giodk for giodk in list_giodk if giodk % 10 == 0]
        elif tiettkb == 5 or tiettkb == 4: result = [giodk for giodk in list_giodk if giodk % 5 == 0]
        elif tiettkb == 3:
            # xem lai cai nay, that ra no khong dung khi random tren giao vien, nen chen them dieu kien vao day
            if self.monhoc.batbuoc is True:
                tmp = [item for item in range(0,60,5)]
                result = [giodk for giodk in list_giodk if giodk in tmp]
            else:
                tmp = [item + 2 for item in range(0, 60, 5)] + [item for item in range(0, 60, 5)]
                result = [giodk for giodk in list_giodk if giodk in tmp]
        elif tiettkb == 2:
            tmp = [item + 3 for item in range(0,60,5)] + [item for item in range(0,60,5)]
            result = [giodk for giodk in list_giodk if giodk in tmp]
        else:
            raise Exception("i dont know")

        if self.monhoc.batbuoc is False:
            final_result = copy.deepcopy(result)
            for i in range(1, 5):
                final_result += copy.deepcopy(result)
            return final_result
        else:
            return result

    @classmethod
    def load_redefineRooms(cls):
        result = OrderedDict()
        with open("thuchanhthuctap.txt", mode="r") as f:
            for line in f.readlines():
                list_mamh, nhom = line.strip().split(";")
                for mamh in list_mamh.split(","):
                    result[mamh] = int(nhom)
        return result

    @classmethod
    def load_phancong(cls, worksheet, dict_lop ,dict_monhoc, dict_nhomph):
        result = []
        ws_pc_rows = worksheet.rows
        first_row = next(ws_pc_rows)
        second_row = next(ws_pc_rows)
        third_row = next(ws_pc_rows)
        for row in ws_pc_rows:
            malop = row[0].value
            loailop = row[1].value
            mamh = row[2].value
            soluongtb = int(row[3].value)
            if loailop == "CQ_CNTN":
                lop = Lop(dict_lop[malop]["CQ"].kh, dict_lop[malop]["CQ"].nganh, malop, "CQ_CNTN", dict_lop[malop]["CNTN"].soluongSV+dict_lop[malop]["CQ"].soluongSV)
            else:
                lop = dict_lop[malop][loailop]
            pc = PhanCong(dict_monhoc[mamh], lop, soluongtb)
            if dict_monhoc[mamh].moriengcntn is True and lop.loailop == "CNTN":
                pc.nhom_phonghoc = 2
            else:
                pc.nhom_phonghoc = dict_monhoc[mamh].nhomph
            pc.available_phonghoc = dict_nhomph[pc.nhom_phonghoc]
            result.append(pc)
        result = sorted(result, key=lambda item: item.monhoc.mamh)
        return result


    def __str__(self):
        return "{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7}".format(self.monhoc.mamh ,self.monhoc.tenmh, self.lop.lop, self.lop.loailop, self.tiettkb, self.soluongSV, self.available_tietbd, self.available_phonghoc)

class BlockElement(object):
    def __init__(self, tietbd, tiettkb, available_phonghoc, monhoc, lop):
        self.tietbd = tietbd
        self.tiettkb = tiettkb
        self.tiethoc = list(range(tietbd, tietbd+tiettkb,1))
        self.available_phonghoc = available_phonghoc
        self.monhoc = monhoc
        self.lop = lop
        self.id_phong = 0
        self.is_open = 0

    @classmethod
    def generate_BlockElements(cls, list_PhanCong):
        result = []
        count = 0
        for pc_id,pc in enumerate(list_PhanCong):
            bes = []
            for tbd in pc.available_tietbd:
                be = BlockElement(tbd, pc.tiettkb, pc.available_phonghoc, pc.monhoc, pc.lop)
                result.append(be)
                bes.append(count)
                count += 1
            list_PhanCong[pc_id].list_beid = bes
        return result, list_PhanCong

    def __str__(self):
        return "{0} | {1} | {2} | {3} | {4} | {5} | {6}".format(self.monhoc.mamh, self.monhoc.tenmh, self.lop.lop, self.lop.loailop, self.tiethoc, self.lop.soluongSV, self.available_phong)


if __name__ == "__main__":

    file_name = "data/datasample.xlsx"
    wb = load_workbook(filename=file_name, data_only=True)
    list_ph, dict_PhongHoc, dict_nhomph = PhongHoc.load_list_PhongHoc(wb["PHÒNG HỌC"])
    list_MonHoc, dict_monhoc = MonHoc.load_list_MonHoc(wb["MÔN HỌC"])
    list_Lop, dict_lop = Lop.load_list_Lop(wb["LỚP"])
    list_giangvien, dict_giangvien = GiangVien.load_list_GiangVien(wb["GIẢNG VIÊN"])

    dict_giangvien, dict_monhoc = GiangVien.load_giodk_GiangVien(dict_giangvien, dict_monhoc, wb["GIẢNG VIÊN GIỜ TRỐNG"])

    list_pc = PhanCong.load_phancong(wb["LỚP MÔN HỌC"], dict_lop, dict_monhoc, dict_nhomph)

    dict_monhocbb2_phancong = OrderedDict()
    dict_monhocnbb_phancong = OrderedDict()
    for i, pc in enumerate(list_pc):
        if pc.monhoc.batbuoc == True:
            if pc.monhoc.mamh not in dict_monhocbb2_phancong.keys():
                dict_monhocbb2_phancong[pc.monhoc.mamh] = []
            dict_monhocbb2_phancong[pc.monhoc.mamh].append(i)
        else:
            if pc.monhoc.mamh not in dict_monhocnbb_phancong.keys():
                dict_monhocnbb_phancong[pc.monhoc.mamh] = []
            dict_monhocnbb_phancong[pc.monhoc.mamh].append(i)

    list_be, list_pc = BlockElement.generate_BlockElements(list_pc)

    print("ahihi")

    # with open("problem.pickle", mode="wb") as f:
    #     pickle.dump((list_be, list_PhanCong, list_PhongHoc, dict_monhocbb2_phancong, dict_monhocnbb_phancong, dict_mon_lop_soluong),f)
    problem = Problem(list_be, list_pc, list_ph, dict_monhocbb2_phancong, dict_monhocnbb_phancong)
    s_0 = problem.init_solution()

    problem.export_solution(s_0, file_name="result/s_0.xlsx")

    print("\n\nSimulated Annealing ... ")

    s_time = time.time()
    s_op = simulated_annealing(problem, s_0)
    e_time = time.time()

    print("Finish simulated annealing in {0} s".format(e_time-s_time))

    problem.export_solution(s_op, file_name="result/{0}.xlsx".format("s_op"))
    problem.export_excel_template(s_op, "reulst/ExcelTemplate.xlsx")









