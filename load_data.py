import openpyxl
from openpyxl import Workbook, load_workbook
from collections import OrderedDict
import numpy as np
import pickle
import copy


class PhongHoc(object):
    col_infor = OrderedDict()
    def __init__(self, day, tenphong, succhua, maychieu, thuchanh, ghichu, tinhchat, nhom):
        self.day = str(day)
        self.tenphong = str(tenphong)
        self.succhua = int(succhua)
        self.maychieu = True if maychieu else False
        self.thuchanh = True if thuchanh else False
        self.ghichu = str(ghichu) if ghichu else ""
        self.tinhchat = str(tinhchat) if tinhchat else ""
        self.nhom = int(nhom)

    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "day":
                result["day"] = idx
            elif col.value == "tenph":
                result["tenph"] = idx
            elif col.value == "succhua":
                result["succhua"] = idx
            elif col.value == "maychieu":
                result["maychieu"] = idx
            elif col.value == "ph":
                result["ph"] = idx
            elif col.value == "thuchanh":
                result["thuchanh"] = idx
            elif col.value == "ghichu":
                result["ghichu"] = idx
            elif col.value == "tinhchat":
                result["tinhchat"] = idx
            elif col.value == "nhom":
                result["nhom"] = idx
        self.col_infor = result
        return result

    @classmethod
    def PhongHoc_from_row(self, row):
        day = row[self.col_infor["day"]].value
        tenphong = row[self.col_infor["tenph"]].value
        succhua = row[self.col_infor["succhua"]].value
        maychieu = row[self.col_infor["maychieu"]].value
        thuchanh = row[self.col_infor["thuchanh"]].value
        ghichu = row[self.col_infor["ghichu"]].value
        tinhchat = row[self.col_infor["tinhchat"]].value
        nhom = row[self.col_infor["nhom"]].value
        return PhongHoc(day, tenphong, succhua,maychieu,thuchanh,ghichu,tinhchat,nhom)

    @classmethod
    def load_list_PhongHoc(cls, worksheet):
        list_PhongHoc = list()
        list_PhongHoc.append(PhongHoc("Dummy","Dummy", 0, None, None, None, None, -1))
        dict_PhongHoc = OrderedDict()
        dict_NhomPhongHoc = OrderedDict()
        ws_phonghoc_rows = worksheet.rows
        first_row = next(ws_phonghoc_rows)
        second_row = next(ws_phonghoc_rows)
        col_infor_row = next(ws_phonghoc_rows)
        PhongHoc.detect_col(col_infor_row)
        print(PhongHoc.col_infor)
        for row in ws_phonghoc_rows:
            ph = PhongHoc.PhongHoc_from_row(row)
            list_PhongHoc.append(ph)
            dict_PhongHoc[ph.tenphong] = ph

        for idx, ph in enumerate(list_PhongHoc):
            if ph.nhom not in dict_NhomPhongHoc.keys():
                dict_NhomPhongHoc[ph.nhom] = []
            dict_NhomPhongHoc[ph.nhom].append(idx)

        return list_PhongHoc, dict_PhongHoc, dict_NhomPhongHoc

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}, {5}, {6}".format(self.day, self.tenphong, self.succhua, self.maychieu, self.thuchanh, self.ghichu, self.tinhchat)

class MonHoc(object):
    col_infor = OrderedDict()
    def __init__(self, mamh, tenmh, dvht, ts, lt, bt, th, tiettkb, nganh, tinhchat, batbuoc):
        self.mamh = str(mamh)
        self.tenmh = str(tenmh)
        self.dvht = int(dvht) if dvht and str(dvht) != "NULL" else 0
        self.ts = int(ts) if ts and str(ts) != "NULL" else 0
        self.lt = int(lt) if lt and str(lt) != "NULL" else 0
        self.bt = int(bt) if bt and str(bt) != "NULL" else 0
        self.th = int(th) if th and str(th) != "NULL" else 0
        self.tiettkb = int(tiettkb) if tiettkb and str(tiettkb) != "NULL" else 0
        # mon nao co so tiet thi gan bang so tiet, neu khong co so tiet thi mac dinh so tiet min
        self.nganh = str(nganh)
        self.tinhchat = str(tinhchat) if tinhchat else ""
        self.batbuoc = True if batbuoc else False
        self.list_giodk = []
        self.list_phonghoccu = []
        self.solopmo = 0

    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "mamh":
                result["mamh"] = idx
            elif col.value == "tenmh":
                result["tenmh"] = idx
            elif col.value == "dvht":
                result["dvht"] = idx
            elif col.value == "ts":
                result["ts"] = idx
            elif col.value == "lt":
                result["lt"] = idx
            elif col.value == "bt":
                result["bt"] = idx
            elif col.value == "th":
                result["th"] = idx
            elif col.value == "tiettkb":
                result["tiettkb"] = idx
            elif col.value == "nganh":
                result["nganh"] = idx
            elif col.value == "tinhchat":
                result["tinhchat"] = idx
            elif col.value == "batbuoc":
                result["batbuoc"] = idx
        self.col_infor = result
        return result

    @classmethod
    def MonHoc_from_row(self, row):
        mamh = row[self.col_infor["mamh"]].value
        tenmh = row[self.col_infor["tenmh"]].value
        dvht = row[self.col_infor["dvht"]].value
        ts = row[self.col_infor["ts"]].value
        lt = row[self.col_infor["lt"]].value
        bt = row[self.col_infor["bt"]].value
        th = row[self.col_infor["th"]].value
        tiettkb = row[self.col_infor["tiettkb"]].value
        nganh = row[self.col_infor["nganh"]].value
        tinhchat = row[self.col_infor["tinhchat"]].value
        batbuoc = row[self.col_infor["batbuoc"]].value
        # if mamh=="AN005":
        #     print("ahihi")
        if tiettkb == "NULL" and tinhchat == "NULL":
            # print(tenmh)
            return None
        return MonHoc(mamh,tenmh,dvht,ts,lt,bt,th,tiettkb,nganh,tinhchat,batbuoc)

    @classmethod
    def load_list_MonHoc(cls, worksheet):
        list_MonHoc = list()
        ws_monhoc_rows = worksheet.rows
        first_row = next(ws_monhoc_rows)
        second_row = next(ws_monhoc_rows)
        col_infor_row = next(ws_monhoc_rows)
        dict_MonHoc = OrderedDict()
        MonHoc.detect_col(col_infor_row)
        print(MonHoc.col_infor)
        for row in ws_monhoc_rows:
            monhoc = MonHoc.MonHoc_from_row(row)
            if monhoc:
                if monhoc.tiettkb == 0:
                    continue
                list_MonHoc.append(monhoc)
                dict_MonHoc[monhoc.mamh] = monhoc
        return list_MonHoc, dict_MonHoc

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}, {5}, {7}, {8}, {9}, {10}".format(self.mamh,
                                                               self.tenmh,
                                                               self.dvht,
                                                               self.ts,
                                                               self.lt,
                                                               self.bt,
                                                               self.th,
                                                               self.tiettkb,
                                                               self.nganh,
                                                               self.tinhchat,
                                                                self.batbuoc)

class Lop(object):
    col_infor = OrderedDict()
    def __init__(self, kh, nganh, lop, loailop, soluongSV):
        self.kh = str(kh)
        self.nganh = str(nganh)
        self.lop = str(lop)
        self.loailop = str(loailop) if loailop else "CQ"
        self.soluongSV = int(soluongSV)
        self.listmonhoc = list()


    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if col.value == "kh":
                result["kh"] = idx
            elif col.value == "nganh":
                result["nganh"] = idx
            elif col.value == "lop":
                result["lop"] = idx
            elif col.value == "loailop":
                result["loailop"] = idx
            elif col.value == "soluongSV":
                result["soluongSV"] = idx
        self.col_infor = result
        return result

    @classmethod
    def Lop_from_row(self, row):
        kh = row[self.col_infor["kh"]].value
        nganh = row[self.col_infor["nganh"]].value
        lop = row[self.col_infor["lop"]].value
        loailop = row[self.col_infor["loailop"]].value
        soluongSV = row[self.col_infor["soluongSV"]].value
        return Lop(kh,nganh,lop,loailop,soluongSV)


    @classmethod
    def load_list_Lop(cls, worksheet):
        list_Lop = list()
        ws_lop_rows = worksheet.rows
        first_row = next(ws_lop_rows)
        second_row = next(ws_lop_rows)
        col_infor_row = next(ws_lop_rows)
        Lop.detect_col(col_infor_row)
        dict_Lop = OrderedDict()
        for row in ws_lop_rows:
            lop = Lop.Lop_from_row(row)
            if lop:
                if lop.lop not in dict_Lop.keys():
                    dict_Lop[lop.lop] = OrderedDict()
                dict_Lop[lop.lop][lop.loailop] = lop
                list_Lop.append(lop)
        return list_Lop, dict_Lop

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}".format(self.kh, self.nganh, self.lop, self.loailop, self.soluongSV)

class TKB(object):
    col_infor = OrderedDict()

    attrs_name = ["id", "mamh", "thu", "tbd", "tkt", "lop", "lopCNTN", "nhom", "soluong", "sotiet", "tiethoc", "phong", "loaiphong", "nganh", "mc", "th", "gheplop_nhom", "ghichu", "phanlop", "phannhom", "hk", "nh", "ngayhoidap", "ngayhoc", "ngaykt", "khong_in", "khongMOLOP"]

    def __init__(self, id=None, mamh=None, thu=None, tbd=None, tkt=None, lop=None, lopCNTN=None, nhom=None, soluong=None, sotiet=None, tiethoc=None, phong=None, loaiphong=None, nganh=None, mc=None, th=None, gheplop_nhom=None, ghichu=None, phanlop=None, phannhom=None, hk=None, nh=None,ngayhoidap=None, ngayhoc=None, ngaykt=None, khong_in=None, khongMOLOP=None):
        self.id = id
        self.mamh = mamh
        self.thu = thu
        self.tbd = tbd
        self.tkt = tkt
        self.lop = lop
        self.lopCNTN = lopCNTN
        self.nhom = nhom
        self.soluong = soluong
        self.sotiet = sotiet
        self.tiethoc = tiethoc
        self.phong = phong
        self.loaiphong = loaiphong
        self.nganh = nganh
        self.mc = mc
        self.th = th
        self.gheplop_nhom = gheplop_nhom
        self.ghichu = ghichu
        self.phanlop = phanlop
        self.phannhom = phannhom
        self.hk = hk
        self.nh = nh
        self.ngayhoidap = ngayhoidap
        self.ngayhoc = ngayhoc
        self.ngaykt = ngaykt
        self.khong_in = khong_in
        self.khongMOLOP = khongMOLOP

    @classmethod
    def detect_col(self, frow):
        result = OrderedDict()
        for idx, col in enumerate(frow):
            if str(col.value) in self.attrs_name:
                result[str(col.value)] = idx
        self.col_infor = result
        return result


    @classmethod
    def TKB_from_row(self, row):
        tkb = TKB()
        for attb in tkb.__dict__.keys():
            tkb.__setattr__(attb,row[self.col_infor[attb]].value)
        return tkb


    @classmethod
    def load_list_TKB(cls, worksheet):
        list_TKB = list()
        ws_TKB_rows = worksheet.rows
        first_row = next(ws_TKB_rows)
        second_row = next(ws_TKB_rows)
        col_infor_row = next(ws_TKB_rows)
        TKB.detect_col(col_infor_row)
        print(TKB.col_infor)
        for row in ws_TKB_rows:
            tkb = TKB.TKB_from_row(row)
            if tkb:
                list_TKB.append(tkb)
            #print(tkb)
        return list_TKB


    def __str__(self):
        return "{0}, {1}, {2}, {3}".format(self.id, self.mamh, self.lop, self.soluong, self.phong, self.loaiphong)

class GiangVien(object):
    def __init__(self, magv, tengv, mamh, list_giodk=[]):
        self.magv = magv
        self.tengv = tengv
        self.mamh = mamh
        self.list_giodk = list_giodk
        self.list_gioday = []
        self.list_gioranh = list_giodk

    @classmethod
    def generate_data(cls, dict_MonHoc):
        list_GiangVien = []
        list_magv = list(range(1000))
        np.random.shuffle(list_magv)
        list_magv = iter(list_magv)
        for mamh in dict_MonHoc.keys():
            monhoc = dict_MonHoc[mamh]
            if monhoc.batbuoc == True:
                no_gv = np.random.randint(6,7)
                for i in range(no_gv):
                    magv = next(list_magv)
                    no_giodk = np.random.randint(3,4)
                    list_giodk = []
                    for j in range(no_giodk):
                        random_date = np.random.randint(0,6)
                        random_sc = np.random.randint(0,2)
                        aaa = list(range(random_date*10+random_sc*5,random_date*10+random_sc*5+5,1))
                        if aaa[0] not in list_giodk:
                            list_giodk+=aaa
                    list_GiangVien.append(GiangVien(magv,"",monhoc.mamh,list_giodk))
                    dict_MonHoc[mamh].list_giodk+=list_giodk
        return list_GiangVien, dict_MonHoc


class PhanCong:

    dict_monhoc_assignbeid = {}
    dict_monhocbb2_random_available_tietbd = {}

    def __init__(self, monhoc, lop, tbd, phonghoc):
        self.monhoc = monhoc
        self.lop = lop
        self.tbd = tbd
        self.phonghoc = phonghoc
        self.tiettkb = self.monhoc.tiettkb
        self.soluongSV = lop.soluongSV
        self.available_tietbd = sorted(self.generate_available_tietbd2())
        self.noBE = len(self.available_tietbd)
        self.list_beid = None
        self.list_assignbeid = []
        self.available_phonghoc = []
        self.nhom_phonghoc = 0

    def generate_available_tietbd(self):
        tiettkb = self.monhoc.tiettkb
        list_giodk = self.monhoc.list_giodk
        if len(list_giodk) == 0 :
            list_giodk = list(range(0,60))
        if tiettkb == 10: return [giodk for giodk in list_giodk if giodk % 10 == 0]
        elif tiettkb == 5 or tiettkb == 4: return [giodk for giodk in list_giodk if giodk % 5 == 0]
        elif tiettkb == 3:
            # xem lai cai nay, that ra no khong dung khi random tren giao vien, nen chen them dieu kien vao day
            tmp = [item + 2 for item in range(0,60,5)] + [item for item in range(0,60,5)]
            return [giodk for giodk in list_giodk if giodk in tmp]
        elif tiettkb == 2:
            tmp = [item + 3 for item in range(0,60,5)] + [item for item in range(0,60,5)]
            return [giodk for giodk in list_giodk if giodk in tmp]
        else:
            raise Exception("i dont know")


    def generate_available_tietbd2(self):
        tiettkb = self.monhoc.tiettkb
        list_giodk = self.monhoc.list_giodk
        result = None
        if tiettkb == 10:
            result =  list(range(0,60,10))
        elif tiettkb == 5 or tiettkb == 4:
            result =  list(range(0,60,5))
        elif tiettkb == 3:
            result = [item + 2 for item in range(0, 60, 5)] + [item for item in range(0, 60, 5)]
        elif tiettkb == 2:
            result = [item + 3 for item in range(0, 60, 5)] + [item for item in range(0, 60, 5)]
        else:
            raise Exception("i dont know")

        if self.monhoc.batbuoc != True:
            final_result = copy.deepcopy(result)
            for i in range(1,5):
                final_result += copy.deepcopy(result)
            return final_result
        else:
            if self.monhoc.mamh in self.dict_monhocbb2_random_available_tietbd:
                return self.dict_monhocbb2_random_available_tietbd[self.monhoc.mamh]
            else:
                final_result = []
                for i in range(self.monhoc.solopmo + 1):
                    rand_idx = np.random.randint(0,len(result))
                    final_result.append(result[rand_idx])
                self.dict_monhocbb2_random_available_tietbd[self.monhoc.mamh] = final_result
                return self.dict_monhocbb2_random_available_tietbd[self.monhoc.mamh]

    @classmethod
    def load_redefineRooms(cls):
        result = OrderedDict()
        with open("thuchanhthuctap.txt", mode="r") as f:
            for line in f.readlines():
                list_mamh, nhom = line.strip().split(";")
                for mamh in list_mamh.split(","):
                    result[mamh] = int(nhom)
        return result

    @classmethod
    def generate_PhanCong(cls, dict_Lop, dict_MonHoc, list_mamonhoc_TN, dict_NhomPhongHoc):
        result = []
        monhoc_nhom = cls.load_redefineRooms()
        for malop in dict_Lop.keys():
            listmonhoc = dict_Lop[malop]["CQ"].listmonhoc
            for mamh in listmonhoc:
                # quyet dinh nhom_phong_hoc
                if mamh in monhoc_nhom.keys():
                    nhom_phong_hoc = monhoc_nhom[mamh]
                else:
                    nhom_phong_hoc = 0

                if dict_MonHoc[mamh].tenmh.find("BT ")!= -1 or dict_MonHoc[mamh].tenmh.find("BÃ i táº­p ")!= -1:
                    nhom_phong_hoc = 4

                available_phonghoc = dict_NhomPhongHoc[nhom_phong_hoc]

                if mamh in list_mamonhoc_TN:
                    pc_cq = PhanCong(dict_MonHoc[mamh], dict_Lop[malop]["CQ"], -1, None)
                    pc_cq.available_phonghoc = available_phonghoc
                    pc_cq.nhom_phonghoc = nhom_phong_hoc
                    result.append(pc_cq)
                    if len(dict_Lop[malop]) == 2:
                        if nhom_phong_hoc == 0:
                            available_phonghoc = dict_NhomPhongHoc[2] # phong hoc cho tai nang
                        pc_tn = PhanCong(dict_MonHoc[mamh], dict_Lop[malop]["CNTN"], -1, None)
                        pc_tn.available_phonghoc = available_phonghoc
                        pc_tn.nhom_phonghoc = 2
                        result.append(pc_tn)
                else:
                    if len(dict_Lop[malop]) == 2:
                        lop_cb = Lop(dict_Lop[malop]["CQ"].kh, dict_Lop[malop]["CQ"].nganh, malop, "CQ_CNTN", dict_Lop[malop]["CNTN"].soluongSV+dict_Lop[malop]["CQ"].soluongSV)
                        pc_cb = PhanCong(dict_MonHoc[mamh], lop_cb, -1, None)
                        pc_cb.available_phonghoc = available_phonghoc
                        pc_cb.nhom_phonghoc = nhom_phong_hoc
                        result.append(pc_cb)
                    else:
                        pc_cq = PhanCong(dict_MonHoc[mamh], dict_Lop[malop]["CQ"], -1, None)
                        pc_cq.available_phonghoc = available_phonghoc
                        pc_cq.nhom_phonghoc = nhom_phong_hoc
                        result.append(pc_cq)

            result = sorted(result, key=lambda item: item.monhoc.mamh)
        return result

    def __str__(self):
        return "{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7}".format(self.monhoc.mamh ,self.monhoc.tenmh, self.lop.lop, self.lop.loailop, self.tiettkb, self.soluongSV, self.available_tietbd, self.available_phonghoc)

class BlockElement(object):
    def __init__(self, tietbd, tiettkb, available_phonghoc, monhoc, lop):
        self.tietbd = tietbd
        self.tiettkb = tiettkb
        self.tiethoc = list(range(tietbd, tietbd+tiettkb,1))
        self.available_phonghoc = available_phonghoc
        self.monhoc = monhoc
        self.lop = lop
        self.id_phong = 0
        self.is_open = 0

    @classmethod
    def generate_BlockElements(cls, list_PhanCong):
        result = []
        count = 0
        for pc_id,pc in enumerate(list_PhanCong):
            bes = []
            for tbd in pc.available_tietbd:
                be = BlockElement(tbd, pc.tiettkb, pc.available_phonghoc, pc.monhoc, pc.lop)
                result.append(be)
                bes.append(count)
                count += 1
            list_PhanCong[pc_id].list_beid = bes
        return result, list_PhanCong

    def __str__(self):
        return "{0} | {1} | {2} | {3} | {4} | {5} | {6}".format(self.monhoc.mamh, self.monhoc.tenmh, self.lop.lop, self.lop.loailop, self.tiethoc, self.lop.soluongSV, self.available_phong)


if __name__ == "__main__":

    file_name = "tkb_khoa_15_16.xlsx"
    wb = load_workbook(filename=file_name, data_only=True)
    list_PhongHoc, dict_PhongHoc, dict_NhomPhongHoc = PhongHoc.load_list_PhongHoc(wb["PHÃ’NG Há»ŒC"])
    list_MonHoc, dict_MonHoc = MonHoc.load_list_MonHoc(wb["MÃ”N Há»ŒC FILTER"])
    list_GiangVien, dict_MonHoc = GiangVien.generate_data(dict_MonHoc)

    list_TKB = TKB.load_list_TKB(wb["THOI KHOA BIEU"])
    list_Lop, dict_Lop = Lop.load_list_Lop(wb["Lá»šP"])

    list_mamonhoc_TN = []
    dict_mon_lop_soluong = OrderedDict()
    #filter tkb: Hoc ky, khongMoLop, nam hoc 2016-2017
    # retrieve information from last tkb
    for tkb in list_TKB:
        if tkb.hk == "1" and tkb.khongMOLOP != "x" and tkb.nh == "16-17":

            # extract the list of monhoc based on lop
            for i in range(len(list_Lop)):
                if str(tkb.lop).find(list_Lop[i].lop) != -1:
                    if tkb.mamh not in list_Lop[i].listmonhoc:
                        list_Lop[i].listmonhoc.append(tkb.mamh)

            # extract monhoc of CNTN
            if tkb.lopCNTN is not None:
                # list_MaMonHoc_TN.append((tkb.mamh, dict_MonHoc[tkb.mamh].tenmh))
                if tkb.mamh not in list_mamonhoc_TN:
                    list_mamonhoc_TN.append(tkb.mamh)

            # extract the list of room
            if tkb.phong not in dict_MonHoc[tkb.mamh].list_phonghoccu:
                dict_MonHoc[tkb.mamh].list_phonghoccu.append(tkb.phong)

            if tkb.mamh in dict_MonHoc.keys():
                dict_MonHoc[tkb.mamh].solopmo += 1



            # extract soluongSV based on mamh_lop_loailop
            tkb_mamh = tkb.mamh
            tkb_lop = tkb.lop[2:-1]
            tkb_loai = "CNTN" if tkb.lopCNTN is not None else "CQ"
            key = "{0}_{1}_{2}".format(tkb_mamh, tkb_lop, tkb_loai)
            if key not in dict_mon_lop_soluong.keys():
                dict_mon_lop_soluong[key] = []
            dict_mon_lop_soluong[key].append(int(tkb.soluong))

    for i in range(len(list_Lop)):
        list_Lop[i].listmonhoc = sorted(list_Lop[i].listmonhoc)


    list_PhanCong = PhanCong.generate_PhanCong(dict_Lop, dict_MonHoc, list_mamonhoc_TN, dict_NhomPhongHoc)

    dict_monhocbb2_phancong = OrderedDict()
    dict_monhocnbb_phancong = OrderedDict()
    for i, pc in enumerate(list_PhanCong):
        # if pc.monhoc.tenmh.find("Thá»±c hÃ nh") != -1 or pc.monhoc.tenmh.find("Thá»±c táº­p") != -1 or pc.monhoc.tenmh.find("TH ") != -1:
        #     print(pc)
        # if pc.monhoc.mamh in list_mamonhoc_TN:
        #     print(pc)
        print(pc)

        if pc.monhoc.batbuoc == True:
            if pc.monhoc.mamh not in dict_monhocbb2_phancong.keys():
                dict_monhocbb2_phancong[pc.monhoc.mamh] = []
            dict_monhocbb2_phancong[pc.monhoc.mamh].append(i)
        else:
            if pc.monhoc.mamh not in dict_monhocnbb_phancong.keys():
                dict_monhocnbb_phancong[pc.monhoc.mamh] = []
            dict_monhocnbb_phancong[pc.monhoc.mamh].append(i)


    print("\n\n----- BE -----")
    #
    list_be, list_PhanCong = BlockElement.generate_BlockElements(list_PhanCong)

    with open("problem.pickle", mode="wb") as f:
        pickle.dump((list_be, list_PhanCong, list_PhongHoc, dict_monhocbb2_phancong, dict_monhocnbb_phancong, dict_mon_lop_soluong),f)







