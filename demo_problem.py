import numpy as np

from load_data import *
from openpyxl import Workbook

import pickle

class Solution(object):
    def __init__(self, sol_solution, list_pc_assignbeid, list_be_phid):
        self.sol_solution = sol_solution
        self.list_pc_assignbeid = list_pc_assignbeid
        self.list_be_phid = list_be_phid

    def clone(self):
        return Solution(copy.deepcopy(self.sol_solution), copy.deepcopy(self.list_pc_assignbeid), copy.deepcopy(self.list_be_phid))


class Problem(object):
    def __init__(self, list_be, list_pc, list_ph, dict_mhbb2_pc, dict_mhnbb_pc):
        self.list_pc = list_pc
        self.list_be = list_be
        self.list_ph = list_ph
        self.dict_mhbb2_pc = dict_mhbb2_pc
        self.dict_mhnbb_pc = dict_mhnbb_pc
        self.list_mhnbb_pcid = []
        for mamh, list_pcid in self.dict_mhnbb_pc.items():
            self.list_mhnbb_pcid+=list_pcid


    def check_tiethoc(cls, list_period, phid, solution):
        for p in list_period:
            if len(solution.sol_solution[phid][p]) != 0:
                return False
        return True

    def assign_be(cls, beid, list_period, phid, solution):
        for p in list_period:
            solution[phid][p].append(beid)
        return solution

    def remove_be(cls, list_period, phid, solution):
        for p in list_period:
            solution[phid][p] = []
        return solution

    def choose_room_for_be(self, be, soluongTB, solution, flexibility = 10, list_choosen_room = [], relaxation = False):
        list_period = be.tiethoc
        result = []

        for phid in be.available_phonghoc:
            # aaa = self.list_ph[phid].succhua >= (soluongTB + flexibility)
            # bbb = self.check_tiethoc(list_period, phid, solution)
            # ccc = phid not in list_choosen_room
            if self.list_ph[phid].succhua >= (soluongTB + flexibility) and phid not in list_choosen_room:
                if relaxation is False:
                    if self.check_tiethoc(list_period, phid, solution):
                        result.append((self.list_ph[phid].succhua, phid))
                else:
                    result.append((self.list_ph[phid].succhua, phid))
        result = sorted(result, key=lambda item: item[0])
        if len(result) == 0:
            return None
        return result[0]

    def check_class_period(self, beid, solution):
        for phid in range(1,len(self.list_ph)):
            for period in self.list_be[beid].tiethoc:
                for tmp_beid in solution.sol_solution[phid][period]:
                    if self.list_be[beid].lop.lop == self.list_be[tmp_beid].lop.lop:
                        if self.list_be[beid].monhoc.mamh != self.list_be[tmp_beid].monhoc.mamh:
                            return False
        return True


    def choose_room_for_listbe(self, cr_listbe, soluongTB, solution, flexibility, mark_beidlist = None, ref_listbeid = [], relaxation = False):
        list_succhua_phid = []
        list_choosen_room = []
        for beid in cr_listbe:

            if mark_beidlist is not None:
                if mark_beidlist[ref_listbeid.index(beid)] == 1:
                    list_succhua_phid.append(None)
                    continue

            if relaxation is False:
                if self.check_class_period(beid, solution) is False:
                    list_succhua_phid.append(None)
                    continue

            succhua_phid = self.choose_room_for_be(self.list_be[beid], soluongTB, solution, flexibility = flexibility, list_choosen_room = list_choosen_room, relaxation = relaxation)
            if succhua_phid is not None:
                list_choosen_room.append(succhua_phid[1])
            list_succhua_phid.append(succhua_phid)
        return list_succhua_phid

    def export_solution(self, solution, file_name=""):

        dict_reversed_beid_pcid = OrderedDict()
        for pc_id, list_assignbeid in enumerate(solution.list_pc_assignbeid):
            for beid in list_assignbeid:
                dict_reversed_beid_pcid[beid] = pc_id


        wb = Workbook()
        wb.remove(wb["Sheet"])
        ws = wb.create_sheet("TKB")
        for j in range(60):
            ws.cell(row=1, column=j + 2, value=str(j))

        for i in range(1, len(self.list_ph)):
            ws.cell(row=i + 1, column=1,
                    value="{0}, {1}, {2}".format(self.list_ph[i].tenphong, self.list_ph[i].succhua,
                                                 self.list_ph[i].tinhchat if self.list_ph[i].tinhchat != "" else "LT"))
            for j in range(60):
                if len(solution.sol_solution[i][j]) > 0:
                    beid = solution.sol_solution[i][j][0]
                    pc_id = dict_reversed_beid_pcid[beid]
                    malop = "{0}_{1}_{2}".format(self.list_pc[pc_id].lop.lop, self.list_pc[pc_id].lop.loailop,
                                                 solution.list_pc_assignbeid[pc_id].index(beid) if len(solution.list_pc_assignbeid[pc_id]) > 1 else "")
                    mamh = self.list_pc[pc_id].monhoc.mamh
                    tenmh = self.list_pc[pc_id].monhoc.tenmh
                    bros = solution.list_pc_assignbeid[pc_id]
                    soluongsv = int(self.list_pc[pc_id].lop.soluongSV / len(solution.list_pc_assignbeid[pc_id]))
                    if j == self.list_be[beid].tietbd:
                        # mamh, lop_nhom, soluongsv
                        ws.cell(row=i + 1, column=j + 2, value=str(solution.sol_solution[i][j]) + "|{0}|{1}|{2}".format(mamh, malop, soluongsv))
                    elif j == self.list_be[beid].tietbd +1:
                        ws.cell(row=i + 1, column=j + 2,
                                value=str(solution.sol_solution[i][j])  + "|{0}|{1}".format(tenmh, str(bros)))
                    else:
                        ws.cell(row=i + 1, column=j + 2,
                                value=str(solution.sol_solution[i][j]))
                else:
                    ws.cell(row=i + 1, column=j + 2, value=str(solution.sol_solution[i][j]))
        wb.save(filename=file_name)

    def check_none_list(self, mylist):
        for item in mylist:
            if item is None:
                return False
        return True

    def try_combine_be_then_insert(self, pcid, solution, mark_beidlist):
        pc = self.list_pc[pcid]
        list_pcid = self.dict_mhbb2_pc[pc.monhoc.mamh]
        pcid_combine = [len(solution.list_pc_assignbeid[pcid]) for pcid in list_pcid]
        check = False

        while check is False:
            max_len = max(pcid_combine)
            if max_len == 1:
                break
            pccb_id = list_pcid[pcid_combine.index(max_len)]

            list_assignbeid = solution.list_pc_assignbeid[pccb_id]
            aaa = [solution.list_be_phid[beid] for beid in list_assignbeid]
            bbb = np.array([self.list_ph[a].succhua for a in aaa], dtype=np.int32)
            sum_bbb = sum(bbb)

            argmin_br1 = np.argmin(bbb)
            succhua_old1 = bbb[argmin_br1]
            bbb[argmin_br1] = 1000000

            argmin_br2 = np.argmin(bbb)
            succhua_old2 = bbb[argmin_br2]
            bbb[argmin_br2] = 1000000

            beid1 = list_assignbeid[argmin_br1]
            beid2 = list_assignbeid[argmin_br2]

            mark_beidlist[self.list_pc[pccb_id].list_beid.index(beid1)] = 0
            mark_beidlist[self.list_pc[pccb_id].list_beid.index(beid2)] = 0

            soluongTB = self.list_pc[pccb_id].lop.soluongSV - (sum_bbb - succhua_old1 - succhua_old2)
            list_succhua_phid = self.choose_room_for_listbe(self.list_pc[pccb_id].list_beid, soluongTB,
                                                                             solution,
                                                                             flexibility=0,
                                                                             mark_beidlist=mark_beidlist, ref_listbeid=self.list_pc[pccb_id].list_beid)
            list_succhua_phid = [(item, best_beid) for item, best_beid in zip(list_succhua_phid,self.list_pc[pccb_id].list_beid)  if item != None ]

            soluongTB2 = self.list_pc[pcid].lop.soluongSV
            list_succhua_phid2 = self.choose_room_for_listbe(self.list_pc[pcid].list_beid, soluongTB2,
                                                             solution,
                                                             flexibility=0,
                                                             mark_beidlist=mark_beidlist,
                                                             ref_listbeid=self.list_pc[pcid].list_beid)
            list_succhua_phid2 = [(item, best_beid) for item, best_beid in
                                  zip(list_succhua_phid2, self.list_pc[pcid].list_beid) if item != None]

            if len(list_succhua_phid) < 2 or len(list_succhua_phid2) < 2:
                mark_beidlist[self.list_pc[pccb_id].list_beid.index(beid1)] = 1
                mark_beidlist[self.list_pc[pccb_id].list_beid.index(beid2)] = 1
                # continue
                pcid_combine[pcid_combine.index(max_len)] = 0
                continue
            else:
                idx = 0
                solution = self.remove_beid_from_solution(solution, pccb_id, beid1)
                solution = self.remove_beid_from_solution(solution, pccb_id, beid2)
                best_succhua_phid, best_beid = list_succhua_phid[idx]
                best_succhua, best_phid = best_succhua_phid
                solution = self.assign_beid_to_solution(solution, pccb_id, best_beid, best_phid)
                mark_beidlist[self.list_pc[pccb_id].list_beid.index(best_beid)] = 1

                best_succhua_phid, best_beid = list_succhua_phid2[1-idx]
                best_succhua, best_phid = best_succhua_phid
                solution = self.assign_beid_to_solution(solution, pcid, best_beid, best_phid)
                mark_beidlist[self.list_pc[pcid].list_beid.index(best_beid)] = 1
                check = True
        return check, solution, mark_beidlist

    def try_combine_be_over_mamh(self, mamh ,solution, mark_beidlist):
        list_pcid = self.dict_mhbb2_pc[mamh]
        for pcid in list_pcid:
            list_assignbeid = solution.list_pc_assignbeid[pcid]

            if len(list_assignbeid) < 2:
                continue

            aaa = [solution.list_be_phid[beid] for beid in list_assignbeid]
            bbb = np.array([self.list_ph[a].succhua for a in aaa], dtype=np.int32)
            sum_bbb = sum(bbb)
            no_space = sum(bbb) - self.list_pc[pcid].lop.soluongSV
            if no_space > 0.5*np.min(bbb):
                argmin_br1 = np.argmin(bbb)
                succhua_old1 = bbb[argmin_br1]
                bbb[argmin_br1] = 1000000

                argmin_br2 = np.argmin(bbb)
                succhua_old2 = bbb[argmin_br2]
                bbb[argmin_br2] = 1000000

                beid1 = list_assignbeid[argmin_br1]
                beid2 = list_assignbeid[argmin_br2]


                mark_beidlist[self.list_pc[pcid].list_beid.index(beid1)] = 0
                mark_beidlist[self.list_pc[pcid].list_beid.index(beid2)] = 0

                soluongTB = self.list_pc[pcid].lop.soluongSV - (sum_bbb-succhua_old1-succhua_old2)
                best_succhua, best_phid, best_beid = self.choose_room_for_listbe(self.list_pc[pcid].list_beid, soluongTB, solution,
                                                                                 flexibility=0,
                                                                                 mark_beidlist=mark_beidlist)
                if best_succhua == 0:
                    mark_beidlist[self.list_pc[pcid].list_beid.index(beid1)] = 1
                    mark_beidlist[self.list_pc[pcid].list_beid.index(beid2)] = 1
                else:
                    solution = self.remove_beid_from_solution(solution, pcid, beid1)
                    solution = self.remove_beid_from_solution(solution, pcid, beid2)
                    solution = self.assign_beid_to_solution(solution, pcid, best_beid, best_phid)
                    mark_beidlist[self.list_pc[pcid].list_beid.index(best_beid)] = 1

        return solution, mark_beidlist

    def check_mark_beidlist(self, mark_beidlist):
        for item in mark_beidlist:
            if item == 0:
                return False
        return True


    def find_k_be(self, k, list_beid, mark_beidlist, finded_beid, relaxation = False):

        start_i = 0
        if relaxation is True:
            start_i = np.random.randint(int(len(list_beid)*0.25), len(list_beid)- k -1)

        for i in range(start_i,len(list_beid)-k+1):
            list_tmp = []
            for j in range(k):
                if self.list_be[list_beid[i+j]].tietbd == self.list_be[list_beid[i]].tietbd and mark_beidlist[i+j] == 0 and list_beid[i+j] not in finded_beid:
                    list_tmp.append(list_beid[i+j])
            if len(list_tmp) == k:
                return list_tmp
        return []

    def choose_room_for_pcid(self, solution, list_khongthephancong, pcid, mark_beidlist, nobe_max, soluongTBMax, try_combine=True, relaxation = False):

        current_succhua = 0
        pc_listbe = self.list_pc[pcid].list_beid
        nobe = nobe_max
        soluongTB = soluongTBMax
        finded_beid = []
        while self.check_mark_beidlist(mark_beidlist) == False:
            tmp_list_beid = self.find_k_be(nobe, pc_listbe, mark_beidlist, finded_beid, relaxation)
            finded_beid += tmp_list_beid
            if len(tmp_list_beid) == 0:
                nobe -= 1
                if nobe == 0:
                    break
                soluongTB = (self.list_pc[pcid].lop.soluongSV - current_succhua) / nobe
            else:
                list_succhua_phicd = self.choose_room_for_listbe(tmp_list_beid, soluongTB, solution, flexibility=0,
                                                                 mark_beidlist=mark_beidlist, ref_listbeid=pc_listbe, relaxation=relaxation)
                count_tmpbeid = 0
                for tmpbeid, succhua_phid in zip(tmp_list_beid, list_succhua_phicd):
                    if succhua_phid is not None:
                        succhua, phid = succhua_phid
                        solution = self.assign_beid_to_solution(solution, pcid, tmpbeid, phid)
                        mark_beidlist[pc_listbe.index(tmpbeid)] = 1
                        current_succhua += succhua
                        count_tmpbeid += 1
                    if current_succhua >= self.list_pc[pcid].lop.soluongSV:
                        break

                if current_succhua < self.list_pc[pcid].lop.soluongSV:
                    nobe -= count_tmpbeid
                    if nobe == 0:
                        break
                    if nobe < nobe_max:
                        soluongTB = (self.list_pc[pcid].lop.soluongSV - current_succhua) / nobe
                else:
                    break

        # Nhung mon chua xep duoc, nen gop lai
        if current_succhua < self.list_pc[pcid].lop.soluongSV:
            # Truong hop nay chi co het gio cua giang vien thoi
            if try_combine is True:
                iscombined, solution, mark_beidlist = self.try_combine_be_then_insert(pcid, solution, mark_beidlist)
                if iscombined is False:
                    list_khongthephancong.append((pcid, nobe_max, soluongTBMax))
                    # print("BB, Xem lai phancong", self.list_pc[pcid].monhoc.mamh, self.list_pc[pcid].lop.lop,
                    #       self.list_pc[pcid].lop.loailop)
            else:
                list_khongthephancong.append((pcid, nobe_max, soluongTBMax))
                # print("KBB, Khong the tim phong va thoi gian thich hop cho phan cong", self.list_pc[pcid].monhoc.mamh, self.list_pc[pcid].lop.lop,
                #           self.list_pc[pcid].lop.loailop)

        return solution, list_khongthephancong, mark_beidlist

    def print_list_pcid(self, list_pcid, solution):
        s_op = solution
        for pcid in list_pcid:
            list_assignbeid = solution.list_pc_assignbeid[pcid]
            # nolecture = len(list_PhanCong[pcid].list_assignbeid)
            # soluongSVperroom = int(list_PhanCong[pcid].lop.soluongSV/nolecture)
            aaa = [s_op.list_be_phid[beid] for beid in list_assignbeid]
            bbb = [self.list_ph[s_op.list_be_phid[beid]].succhua for beid in list_assignbeid]
            ccc = list_assignbeid
            ddd = [self.list_be[beid].tietbd for beid in list_assignbeid]
            print(pcid, self.list_pc[pcid].monhoc.mamh, self.list_pc[pcid].monhoc.tiettkb,
                  self.list_pc[pcid].monhoc.tenmh, self.list_pc[pcid].lop.lop,
                  self.list_pc[pcid].lop.loailop, aaa, bbb, self.list_pc[pcid].lop.soluongSV,
                  "warning" if sum(bbb) < self.list_pc[pcid].lop.soluongSV else "", ccc, ddd)

    def init_batbuoc(self):
        # init solution
        sol_solution = []
        for i in range(len(self.list_ph)):
            tmp = []
            for j in range(60):
                tmp.append([])
            sol_solution.append(tmp)
        list_pc_assignbeid = [[] for i in range(len(self.list_pc))]
        list_be_phid = [0]*len(self.list_be)
        solution = Solution(sol_solution, list_pc_assignbeid, list_be_phid)
        list_khongthephancong = []

        list_mmbb2_pc = sorted(self.dict_mhbb2_pc.items(), key=lambda item: len(self.list_pc[item[1][0]].list_beid))

        for mamh, list_pcid in list_mmbb2_pc:
            mark_beidlist = [0]*len(self.list_pc[list_pcid[0]].list_beid)

            list_pcid_nobe = []

            for pcid in list_pcid:
                soluongTB = self.list_pc[pcid].soluongtb
                nobe = int(np.ceil(self.list_pc[pcid].lop.soluongSV / soluongTB))
                list_pcid_nobe.append((pcid, nobe, soluongTB))

            list_pcid_nobe = sorted(list_pcid_nobe, key=lambda item: item[1], reverse=True)

            for pcid, nobe_max, soluongTBMax in list_pcid_nobe:
                # if mamh == "BAA00004" and self.list_pc[pcid].lop.lop == "16TTH1":
                #     print("init_batbuoc for debug")

                solution, list_khongthephancong, mark_beidlist = self.choose_room_for_pcid(solution, list_khongthephancong, pcid, mark_beidlist, nobe_max, soluongTBMax,
                                                                            try_combine = True, relaxation=False)

            self.print_list_pcid(list_pcid, solution)

        return solution, list_khongthephancong

    def remove_pcid(self, pcid, solution):
        tmp_list_beid = copy.deepcopy(solution.list_pc_assignbeid[pcid])
        for beid in tmp_list_beid:
            solution = self.remove_beid_from_solution(solution, pcid, beid)
        return solution

    def init_khongbatbuoc(self, solution, list_khongthephancong):

        list_nmmbb_pc = sorted(self.dict_mhnbb_pc.items(), key=lambda item: len(self.list_pc[item[1][0]].list_beid))
        dict_save = {}
        for mamh, list_pcid in list_nmmbb_pc:
            list_pcid_nobe = []
            # danh gia so luong lop can chia va so luong tb cho tung pcid
            for pcid in list_pcid:
                soluongTB = self.list_pc[pcid].soluongtb
                soluongTB = min(soluongTB,
                                max([self.list_ph[ph_id].succhua for ph_id in self.list_pc[pcid].available_phonghoc]))

                nobe = int(np.ceil(self.list_pc[pcid].lop.soluongSV / soluongTB))
                list_pcid_nobe.append((pcid, nobe, soluongTB))

            # sort theo soluong lop can chia, lop co so luong chia lop thi can xep truoc
            list_pcid_nobe = sorted(list_pcid_nobe, key=lambda item: item[1], reverse=True)

            for pcid, nobe_max, soluongTBMax in list_pcid_nobe:
                # if mamh == "CTT006" and self.list_pc[pcid].lop.lop == "15CTT1" and self.list_pc[pcid].lop.loailop == "CNTN":
                #     print("for debugging")

                if pcid == 202:
                    print("ttdt")

                mark_beidlist = [0]*len(self.list_pc[pcid].list_beid)

                solution, list_khongthephancong, mark_beidlist = self.choose_room_for_pcid(solution,
                                                                                           list_khongthephancong, pcid,
                                                                                           mark_beidlist, nobe_max,
                                                                                           soluongTBMax,
                                                                                           try_combine=False, relaxation=False)

            self.print_list_pcid(list_pcid, solution)

        return solution, list_khongthephancong


    def print_all_tiethoc(self, pcid, solution):
        malop = self.list_pc[pcid].lop.lop
        result = []
        result_fn = []
        for i in range(1, len(solution.sol_solution)):
            for j in range(0, 60):
                for k in solution.sol_solution[i][j]:
                    if self.list_be[k].lop.lop == malop:
                        if j not in result:
                            result.append(j)

        for j in range(0, 60):
            if j not in result:
                result_fn.append(j)
        print(result_fn)





    def init_solution(self):
        print("Xep mon hoc bat buoc")
        solution, list_khongthephancong = self.init_batbuoc()
        print("Xep mon hoc khong bat buoc")
        solution, list_khongthephancong = self.init_khongbatbuoc(solution, list_khongthephancong)

        if len(list_khongthephancong) > 0:
            print("\n\n KHONG THE PHAN CONG")
            self.print_list_pcid([pcid for pcid, nobe_max, soluongTBMax in list_khongthephancong], solution)

        print("Objective function")
        print(self.objective_function(solution))


        list_khongthephancong2 = []
        for pcid, nobe_max, soluongTBMax in list_khongthephancong:
            mark_beidlist = [0] * len(self.list_pc[pcid].list_beid)
            solution, list_khongthephancong2, mark_beidlist = self.choose_room_for_pcid(solution, list_khongthephancong2, pcid, mark_beidlist, nobe_max,soluongTBMax,try_combine=False, relaxation=True)
        if len(list_khongthephancong2) > 0:
            print("\n\n KHONG THE PHAN CONG 2")
            self.print_list_pcid([pcid for pcid, nobe_max, soluongTBMax in list_khongthephancong2], solution)

        print("Objective function")
        print(self.objective_function(solution))

        return solution

    def hard_constraint_1(self, solution):
        no_violation = 0
        list_violation = []
        for i in range(1, len(solution.sol_solution)):
            for j in range(len(solution.sol_solution[i])):
                if len(solution.sol_solution[i][j]) > 1:
                    no_violation += (len(solution.sol_solution[i][j])-1)
                    list_violation.append((i, j, solution.sol_solution[i][j]))
        return no_violation, list_violation

    def hard_constraint_2(self, solution):
        no_violation = 0
        list_violation = []
        for pc_id, list_assignbeid in enumerate(solution.list_pc_assignbeid):
            if len(list_assignbeid) > 0:
                if sum([self.list_ph[solution.list_be_phid[be_id]].succhua for be_id in list_assignbeid]) < self.list_pc[pc_id].lop.soluongSV:
                    no_violation += 1
                    list_violation.append((pc_id, list_assignbeid, [solution.list_be_phid[be_id] for be_id in list_assignbeid]))
        return no_violation, list_violation

    def internset_listlist(self, listlist):
        if len(listlist) == 1:
            return []
        result = set(listlist[0])
        for i in range(1, len(listlist)):
            result &= set(listlist[i])
        return result

    def hard_constraint_3(self, solution):
        no_violation = 0
        list_violation = []
        for k in range(0, 60, 5):
            dict_lop_monhoc_giohoc = OrderedDict()
            for i in range(1, len(solution.sol_solution)):
                for j in range(0, 5):
                    if len(solution.sol_solution[i][k + j]) > 0:
                        for be_id in solution.sol_solution[i][k + j]:
                            malop = self.list_be[be_id].lop.lop
                            mamh = self.list_be[be_id].monhoc.mamh

                            if malop not in dict_lop_monhoc_giohoc:
                                dict_lop_monhoc_giohoc[malop] = {}
                            if mamh not in dict_lop_monhoc_giohoc[malop]:
                                dict_lop_monhoc_giohoc[malop][mamh] = []
                            if k + j not in dict_lop_monhoc_giohoc[malop][mamh]:
                                dict_lop_monhoc_giohoc[malop][mamh].append(k + j)

            dict_lop_monhoc_giohoc = sorted(dict_lop_monhoc_giohoc.items(), key=lambda item: item[0])

            for malop, dsmhtg in dict_lop_monhoc_giohoc:
                listlist = [listgiohoc for mamh, listgiohoc in dsmhtg.items()]
                for i in range(len(listlist)):
                    for j in range(i+1, len(listlist)):
                        if len(set(listlist[i]) & set(listlist[j])) > 0:
                            no_violation += 1
                            list_violation.append((malop, listlist))
        return no_violation, list_violation

    def soft_constraint_1(self, solution):
        # số lượng phòng trống trong cả một buổi học
        no_violation = 0
        list_violation = []
        for i in range(1, len(solution.sol_solution)):
            # TODO: should care about this
            if self.list_ph[i].tinhchat == "TATC" or self.list_ph[i].tinhchat == "SAN":
                continue
            for j in range(0, 60, 5):
                if sum([len(solution.sol_solution[i][j + k]) for k in range(5)]) == 0:
                    no_violation += 1
                    list_violation.append((i, j))
        return no_violation, list_violation

    def soft_constraint_2(self, solution):
        no_violation = 0
        list_violation = []
        # giờ lũng của lớp học
        for k in range(0, 60, 10):
            dict_lop_giohoc = OrderedDict()
            for i in range(1, len(solution.sol_solution)):
                for j in range(0, 10):
                    if len(solution.sol_solution[i][k + j]) > 0:
                        for be_id in solution.sol_solution[i][k + j]:
                            malop = self.list_be[be_id].lop.lop
                            if malop not in dict_lop_giohoc.keys():
                                dict_lop_giohoc[malop] = []

                            if (k + j) not in dict_lop_giohoc[malop]:
                                dict_lop_giohoc[malop].append((k + j, be_id))

            for lop, list_giohoc_phonghoc in dict_lop_giohoc.items():
                list_giohoc = sorted(list(set([giohoc_phonghoc[0] for giohoc_phonghoc in list_giohoc_phonghoc])))
                list_giohoc.insert(k + 0, 0)
                list_giohoc.append(k + 9)
                sub_violation = 0
                for p in range(len(list_giohoc) - 1):
                    if list_giohoc[p + 1] - list_giohoc[p] >= 5:
                        sub_violation += 1

                if sub_violation > 0:
                    no_violation += sub_violation
                    list_violation.append(list(set([giohoc_phonghoc[1] for giohoc_phonghoc in list_giohoc_phonghoc])))

        return no_violation, list_violation

    def soft_constraint_3(self, solution, flexibility = 100):
        no_violation = 0
        list_violation = []
        for pc_id, list_assignbeid in enumerate(solution.list_pc_assignbeid):
            if len(list_assignbeid) > 0:
                no_space = sum([self.list_ph[solution.list_be_phid[be_id]].succhua for be_id in list_assignbeid]) - self.list_pc[pc_id].lop.soluongSV
                if no_space > flexibility:
                    no_violation += 1
                    list_violation.append(pc_id)
        return no_violation, list_violation

    def objective_function(self, solution):
        self.wa1, self.wa2, self.wa3, self.wb1, self.wb2, self.wb3 = 1000,1000,1000,1,1,1
        a1, la1 = self.hard_constraint_1(solution)
        a2, la2 = self.hard_constraint_2(solution)
        a3, la3 = self.hard_constraint_3(solution)
        b1, lb1 = self.soft_constraint_1(solution)
        b2, lb2 = self.soft_constraint_2(solution)
        b3, lb3 = self.soft_constraint_3(solution)
        return self.wa1*a1 + self.wa2*a2 + self.wa3*a3  + self.wb1*b1 + self.wb2*b2 + self.wb3*b3

    def remove_beid_from_solution(self, solution, pcid, beid):
        list_period = self.list_be[beid].tiethoc
        phid = solution.list_be_phid[beid]
        for p in list_period:
            solution.sol_solution[phid][p].remove(beid)
        solution.list_pc_assignbeid[pcid].remove(beid)
        solution.list_be_phid[beid] = 0
        return solution

    def assign_beid_to_solution(self, solution, pcid, beid, phid):
        list_period = self.list_be[beid].tiethoc
        for p in list_period:
            solution.sol_solution[phid][p].append(beid)
        solution.list_pc_assignbeid[pcid].append(beid)
        solution.list_be_phid[beid] = phid
        return solution

    def check_available_phid_on_solution(self, solution, beid, phid):
        list_period = self.list_be[beid].tiethoc
        for p in list_period:
            if len(solution.sol_solution[phid][p]) != 0:
                return False
        return True

    def find_list_phid_on_solution(self, solution, pcid, list_beid, flexibility = 0):
        result = []
        for beid in list_beid:
            choosen_phid = self.find_phid_on_solution(solution, pcid, beid, flexibility=flexibility, choosen_listphid=result)
            if choosen_phid is not None:
                result.append(choosen_phid)
        return result

    def find_phid_on_solution(self, solution, pcid, beid, flexibility = 0, choosen_listphid = [] ):
        soluongTB = int(self.list_pc[pcid].lop.soluongSV/len(solution.list_pc_assignbeid[pcid]))
        result = []
        for phid in self.list_be[beid].available_phonghoc:
            if self.list_ph[phid].succhua > soluongTB and self.list_ph[phid].succhua < soluongTB + flexibility and self.check_available_phid_on_solution(solution, beid, phid):
                result.append(phid)
        if len(result) == 0:
            return None
        if len(set(result) & set(choosen_listphid)) == len(result):
            return None

        choosen_phid = result[np.random.randint(0,len(result))]
        while choosen_phid in choosen_listphid:
            choosen_phid = result[np.random.randint(0, len(result))]
        return choosen_phid

    def find_beid_in_list_beid_by_tietbd(self, tietbd, list_beid):
        for beid in list_beid:
            if tietbd == self.list_be[beid].tietbd:
                return beid
        return -1

    def find_k_beid_in_list_beid_by_tietbd(self, tietbd, k, list_beid):
        result = []
        for beid in list_beid:
            if tietbd == self.list_be[beid].tietbd:
                result.append(beid)
            if len(result) == k:
                break
        return result

    def choose_swap_be(self, tmp_solution):
        while True:
            rand_pcid1 = self.list_mhnbb_pcid[np.random.randint(0, len(self.list_mhnbb_pcid))]
            if len(tmp_solution.list_pc_assignbeid[rand_pcid1]) == 0:
                continue
            rand_beid1 = tmp_solution.list_pc_assignbeid[rand_pcid1][np.random.randint(0, len(tmp_solution.list_pc_assignbeid[rand_pcid1]))]
            list_rand_beid1 = [beid for beid in tmp_solution.list_pc_assignbeid[rand_pcid1] if self.list_be[beid].tietbd == self.list_be[rand_beid1].tietbd]
            tbd_beid1 = self.list_be[rand_beid1].tietbd
            tiettkb_beid1 = self.list_be[rand_beid1].tiettkb


            rand_pcid2  = self.list_mhnbb_pcid[np.random.randint(0, len(self.list_mhnbb_pcid))]
            if len(tmp_solution.list_pc_assignbeid[rand_pcid2]) == 0:
                continue
            rand_beid2 = tmp_solution.list_pc_assignbeid[rand_pcid2][np.random.randint(0, len(tmp_solution.list_pc_assignbeid[rand_pcid2]))]
            list_rand_beid2 = [beid for beid in tmp_solution.list_pc_assignbeid[rand_pcid2] if self.list_be[beid].tietbd == self.list_be[rand_beid2].tietbd]
            tbd_beid2 = self.list_be[rand_beid2].tietbd
            tiettkb_beid2 = self.list_be[rand_beid2].tiettkb

            # check xem tietbd cua thang, thang kia da phan cong chua
            a = self.find_beid_in_list_beid_by_tietbd(tbd_beid1, tmp_solution.list_pc_assignbeid[rand_pcid2])
            c = self.find_beid_in_list_beid_by_tietbd(tbd_beid2, tmp_solution.list_pc_assignbeid[rand_pcid1])

            list_swaped_beid2 = self.find_k_beid_in_list_beid_by_tietbd(tbd_beid1, len(list_rand_beid2) ,self.list_pc[rand_pcid2].list_beid)
            list_swaped_beid1 = self.find_k_beid_in_list_beid_by_tietbd(tbd_beid2, len(list_rand_beid1) ,self.list_pc[rand_pcid1].list_beid)

            if a == -1 and c == -1 and  len(list_swaped_beid2) != 0 and len(list_swaped_beid1) != -0:
                return rand_pcid1, list_rand_beid1, list_swaped_beid1, rand_pcid2, list_rand_beid2, list_swaped_beid2
                # print(rand_pcid1, rand_beid1, swaped_beid1, rand_pcid2, rand_beid2, swaped_beid2)

    def remove_list_beid_from_solution(self, tmp_solution, pcid, list_beid):
        for beid in list_beid:
            tmp_solution = self.remove_beid_from_solution(tmp_solution, pcid, beid)
        return tmp_solution

    def assign_list_beid_to_solution(self, tmp_solution, pcid, list_beid, list_phid):
        for beid, phid in zip(list_beid, list_phid):
            tmp_solution = self.assign_beid_to_solution(tmp_solution,pcid,beid,phid)
        return tmp_solution

    def swap_move(self, solution):
        tmp_solution = solution.clone()
        while True:
            rand_pcid1, list_rand_beid1, list_swaped_beid1, rand_pcid2, list_rand_beid2, list_swaped_beid2 = self.choose_swap_be(tmp_solution)

            tmp_list_phid1 = self.find_list_phid_on_solution(tmp_solution, rand_pcid1, list_swaped_beid1, flexibility=100)
            tmp_list_phid2 = self.find_list_phid_on_solution(tmp_solution, rand_pcid2, list_swaped_beid2, flexibility=100)

            if len(tmp_list_phid1) == len(list_swaped_beid1) and len(tmp_list_phid2) == len(list_swaped_beid2):

                intersect_ph = set(tmp_list_phid1) & set(tmp_list_phid2)
                intersect_tiethoc = set(self.list_be[list_swaped_beid1[0]].tiethoc) & set(self.list_be[list_swaped_beid2[0]].tiethoc)
                if len(intersect_ph) > 0 and len(intersect_tiethoc) > 0:
                    continue
                tmp_solution =  self.remove_list_beid_from_solution(tmp_solution, rand_pcid1, list_rand_beid1)
                tmp_solution =  self.assign_list_beid_to_solution(tmp_solution, rand_pcid1, list_swaped_beid1, tmp_list_phid1)
                tmp_solution =  self.remove_list_beid_from_solution(tmp_solution, rand_pcid2, list_rand_beid2)
                tmp_solution =  self.assign_list_beid_to_solution(tmp_solution, rand_pcid2, list_swaped_beid2, tmp_list_phid2)
                return tmp_solution

    def print_solution(self, s_op):
        for pcid, list_assignbeid in enumerate(s_op.list_pc_assignbeid):
            # nolecture = len(list_PhanCong[pcid].list_assignbeid)
            # soluongSVperroom = int(list_PhanCong[pcid].lop.soluongSV/nolecture)
            aaa = [s_op.list_be_phid[beid] for beid in list_assignbeid]
            bbb = [self.list_ph[s_op.list_be_phid[beid]].succhua for beid in list_assignbeid]
            print(self.list_pc[pcid].monhoc.mamh, self.list_pc[pcid].monhoc.tenmh, self.list_pc[pcid].lop.lop,
                  self.list_pc[pcid].lop.loailop, aaa, bbb, self.list_pc[pcid].lop.soluongSV)

    def write_row_to_worksheet(self, ws, row_id, values):
        for col_id, value in enumerate(values):
            ws.cell(row=row_id, column=col_id+1, value=str(value))
        return ws

    def export_excel_template(self, solution, file_name):
        wb = Workbook()
        wb.remove(wb["Sheet"])
        ws = wb.create_sheet("TKB")
        ws.cell(row=1, column=1, value="TKB")
        second_row_values = ["id","Mã môn học","Thứ","Tiết bắt đầu","Tiết kết thúc","Lớp","Lớp CNTN","Nhóm","Số lượng","Số tiết","Tiết học","Phòng","Loại phòng","Ngành","Máy chiếu","Thực hành"]
        third_row_values = ["id","mamh","thu","tbd","tkt","lop","lopCNTN","nhom","soluong","sotiet","tiethoc","phong","loaiphong","nganh","mc","th"]
        ws = self.write_row_to_worksheet(ws, 2, second_row_values)
        ws = self.write_row_to_worksheet(ws, 3, third_row_values)
        stt = 4
        for pcid, list_assignbeid in  enumerate(solution.list_pc_assignbeid):
            if len(list_assignbeid) == 0:
                continue
            elif len(list_assignbeid) == 1:
                beid = list_assignbeid[0]
                values = [stt,
                          self.list_be[beid].monhoc.mamh,
                          int(self.list_be[beid].tietbd / 10) + 2,
                          (self.list_be[beid].tiethoc[0] % 10) + 1,
                          (self.list_be[beid].tiethoc[-1] % 10) + 1,
                          self.list_be[beid].lop.lop,
                          "CNTN" if self.list_be[beid].lop.loailop == "CNTN" else "",
                          "",
                          self.list_be[beid].lop.soluongSV,
                          len(self.list_be[beid].tiethoc),
                          "{0}-{1}".format((self.list_be[beid].tiethoc[0] % 10) + 1,
                                           (self.list_be[beid].tiethoc[-1] % 10) + 1),
                          self.list_ph[solution.list_be_phid[beid]].tenphong,
                          "LT" if self.list_ph[solution.list_be_phid[beid]].tinhchat == "" else self.list_ph[
                              solution.list_be_phid[beid]].tinhchat,
                          self.list_be[beid].monhoc.nganh,
                          "x" if self.list_ph[solution.list_be_phid[beid]].maychieu == True else "",
                          "x" if self.list_ph[solution.list_be_phid[beid]].thuchanh == True else ""]
                ws = self.write_row_to_worksheet(ws, stt, values)
                stt +=1
            else:
                for nhom, beid in enumerate(list_assignbeid):
                    values = [stt,
                              self.list_be[beid].monhoc.mamh,
                              int(self.list_be[beid].tietbd / 10) + 2,
                              (self.list_be[beid].tiethoc[0] % 10) + 1,
                              (self.list_be[beid].tiethoc[-1] % 10) + 1,
                              self.list_be[beid].lop.lop,
                              "CNTN" if self.list_be[beid].lop.loailop == "CNTN" else "",
                              nhom,
                              int((self.list_be[beid].lop.soluongSV)/len(list_assignbeid))+1,
                              len(self.list_be[beid].tiethoc),
                              "{0}-{1}".format((self.list_be[beid].tiethoc[0] % 10) + 1,
                                               (self.list_be[beid].tiethoc[-1] % 10) + 1),
                              self.list_ph[solution.list_be_phid[beid]].tenphong,
                              "LT" if self.list_ph[solution.list_be_phid[beid]].tinhchat == "" else self.list_ph[
                                  solution.list_be_phid[beid]].tinhchat,
                              self.list_be[beid].monhoc.nganh,
                              "x" if self.list_ph[solution.list_be_phid[beid]].maychieu == True else "",
                              "x" if self.list_ph[solution.list_be_phid[beid]].thuchanh == True else ""]
                    ws = self.write_row_to_worksheet(ws, stt, values)
                    stt+=1
        wb.save(file_name)

def simulated_annealing(problem, s_0, Itermax = 500):
    s_current = s_0
    s_best  = s_current
    T_0 = 100
    T   = T_0
    Iter = 0
    Niter = 0
    Nitermax = 20
    Stop = False
    MinorItermax = 200
    ob_s_best = problem.objective_function(s_best)
    while Stop == False:
        Iter +=1
        Niter+=1
        MinorIter = 0
        while MinorIter <= MinorItermax:
            MinorIter +=1
            s_ = problem.swap_move(s_current)
            ob_s_ = problem.objective_function(s_)
            ob_s_current = problem.objective_function(s_current)
            delta_f = ob_s_ - ob_s_current
            if delta_f < 0:
                s_current = s_
                if ob_s_current < ob_s_best:
                    s_best = copy.deepcopy(s_current)
                    ob_s_best = ob_s_current
                    Niter = 0
            else:
                rand = float(np.random.randint(1,101)/100)
                if rand < np.exp(-delta_f/T):
                    s_current = copy.deepcopy(s_)
        print(Iter, Niter, MinorIter, T, ob_s_best)
        if Niter == Nitermax:
            T = 100
            Niter=0
        else:
            T*=rand
        if Iter == Itermax:
            Stop = True
        if problem.objective_function(s_current) ==0:
            Stop = True
    return s_best


import time

import argparse






