### The University Course Timetabling Problem ###

## Requirements ##
* Python 3
* numpy
* openpyxl

### Input ###
* data/datasample.xlsx

### Running ###
python3 demo.py


* This step will read classrooms, courses, classes, and assignments from the input file and create block elements.
* Finally, the system uses simulated annealing to find an optimized solution.
* When the system finishes the optimization, It generates the output at "result/ExcelTemplate.xlsx".


### Output ###


* result/ExcelTemplate.xlsx