import pickle
from load_data import *
from Problem import *
from openpyxl import load_workbook, Workbook



if __name__ == "__main__":
    with open("problem.pickle", mode="rb") as f:
        list_be, list_pc, list_ph, dict_mhbb2_pc, dict_mhnbb_pc, dict_mon_lop_soluong = pickle.load(f)

    problem = Problem(list_be, list_pc, list_ph, dict_mhbb2_pc, dict_mhnbb_pc, dict_mon_lop_soluong)

    # with open("/Users/hynguyen/Desktop/result/op2.pickle", mode="rb") as f:
    #     s_op =  pickle.load(f)
    #
    # print(problem.objective_function(s_op))
    # problem.export_excel_template(s_op, "result/ExcelTemplate.xlsx")
    #